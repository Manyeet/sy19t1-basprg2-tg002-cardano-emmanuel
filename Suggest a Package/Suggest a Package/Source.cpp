#include <iostream>
#include <string>
#include <time.h>
#include <stdlib.h>


using namespace std;
int startQ(int a, bool b)
{
	system("CLS");
	cout << "Your gold: " << a << endl;
	cout << "Would you like to make a purchase? " << endl;
	cout << endl << "1 = YES || 0 = NO" << endl << "Your answer: ";
	cin >> b;
	return b;
}
void sortFunc(bool a, int b[], int c) // a is check, b is package, c is hold
{
	for (int i = 0; i < 6; i++)
	{
		a = 0;
		for (int j = 0; j < 6; j++)
		{
			if (b[j + 1] < b[j])
			{
				c = b[j];
				b[j] = b[j + 1];
				b[j + 1] = c;
				a = 1;
			}
		}
	}
}
int bestIAP(int i, int a[], int b[]) // a is packages, b is hardCurr
{
	int c = -1;
	if (b[0] > a[i])
	{
		c = 1;
		c = b[0];
	}
	else if (b[1] > a[i])
	{
		c = 1;
		c = b[1];
	}
	else if (b[2] > a[i])
	{
		c = 1;
		c = b[2];
	}
	else if (b[3] > a[i])
	{
		c = 1;
		c = b[3];
	}
	if (c >= 0)
	{
		return c;
	}
}
void showChoices(int a[])
{
	for (int i = 0; i <= 6; i++)
	{
		cout << i + 1 << " " << a[i] << endl;
	}
}
int choose(int a, int b[], int c)
{
	system("CLS");
	cout << "Your gold: " << a << endl;
	cout << "What would you like to buy?" << endl;
	cout << "Your options:" << endl << "Package #" << endl;
	showChoices(b);
	cout << endl << "Enter package number # (1-7):" << "(0 - Cancel and leave)" << endl << "Your answer: ";
	cin >> c;
	return c;
}
int chosen(int i, int a, int b, int c[], bool d)
{
	i = b, i = i - 1;
	system("CLS");
	cout << "Your gold: " << a << endl;
	cout << "Your chosen package #" << b << " costs " << c[i] << " gold." << endl;
	system("pause"), system("CLS");
	cout << "Your gold: " << a << endl;
	cout << "Your chosen package #" << b << " costs " << c[i] << " gold." << endl;
	cout << "Buy now?" << endl << endl << "1 = YES || 0 = NO" << endl << "Your answer: ";
	cin >> d;
	return i;
}
int insuff(int a, int b, bool d)
{
	system("CLS");
	cout << "Your gold: " << a << endl;
	cout << "You have insufficient funds." << endl;
	system("pause"), system("CLS");
	cout << "Your gold: " << a << endl;
	cout << "You have insufficient funds." << endl;
	cout << "We suggest that you should avail of one of our deals that would help suffice the needed amount?" << endl;
	system("pause"), system("CLS");
	cout << "Your gold: " << a << endl;
	cout << "You have insufficient funds." << endl;
	cout << "We suggest that you should avail of one of our deals that would help suffice the needed amount?" << endl;
	cout << "Perhaps this package would suffice: " << b << endl;
	system("pause"), system("CLS");
	cout << "Your gold: " << a << endl;
	cout << "You have insufficient funds." << endl;
	cout << "We suggest that you should avail of one of our deals that would help suffice the needed amount?" << endl;
	cout << "Perhaps this package would suffice: " << b << endl;
	cout << "Would you like to avail of this package?" << endl << endl << "1 = YES || 0 = NO" << endl << "Your answer: ";
	cin >> d;
	return d;
}
/*int trans(int i, int a, int b[]) // gold is a, b is packages,
{
	system("CLS");
	int deductedGold = b[i];
	cout << deductedGold << " gold will now be deducted from your gold" << endl;
	cout << "You now have: " << a << " gold" << endl;
	system("pause");
	return a; 
} */
void no()
{
	cout << "Thank you for your patronage." << endl;
	system("pause");
	system("CLS");
	cout << "This app will now exit." << endl;
}

int main()
{
	bool check = 1, ans = 0;
	int hold = 0, gold = 250, i = 0;
	int packages[] = { 30750, 1780, 500, 250000, 13333, 150, 4050 };
	string inApp[] = { "Bag of Gold", "Bags of Gold", "Stash of Gold", "Gold Vault" };
	int cost[] = { 0.99, 2.99, 9.99, 49.99 };
	int hardCurr[] = { 1000, 5000, 30000, 200000 };
	sortFunc(check, packages, hold);

	while (gold >= 1)
	{
		ans = startQ(gold, ans);
		if (ans >= 1)
		{
			hold = choose(gold, packages, hold);
			while (hold >= 7)
			{
				cout << endl << "Requirements cannot be met." << endl << "There is no available package to proceed unto the transaction." << endl;
				hold--;
				cout << "Your chosen package costs " << packages[hold] << " gold." << endl;
				system("pause");
				ans = startQ(gold, ans);
				if (ans == 1)
				{
					system("CLS");
					hold = choose(gold, packages, hold);
				}
				else if (ans == 0)
				{
					no();
					break;
				}
			}
		}
		else if (ans == 0)
		{
			no();
			break;
		}
		if (hold >= 1)
		{
			i = chosen(i, gold, hold, packages, ans);
			if (ans >= 1)
			{
				if (packages[i] <= gold)
				{
					gold = gold - packages[i];
					system("CLS");
					cout << packages[i] << " gold will now be deducted from your gold" << endl;
					cout << "You now have: " << gold << " gold" << endl;
					system("pause");
				}
				else if (packages[i] >= gold)
				{
					hold = bestIAP(i, packages, hardCurr);
					ans = insuff(gold, hold, ans);
					if (ans == 1)
					{
						cout << "Your gold: " << gold << endl;
						gold = gold + hold;
						cout << hold << " will now be added to your current gold" << endl;
						system("pause"), system("CLS");
						cout << "Your gold: " << gold << endl;
						system("pause");
						gold = gold - packages[i];
						system("CLS");
						cout << packages[i] << " gold will now be deducted from your gold" << endl;
						cout << "You now have: " << gold << " gold" << endl;
						system("pause");
					}
					else if (ans == 0)
					{
						no();
						break;
					}
				}
			}
			else if (ans < 1)
			{
				no();
				break;
			}
		}
		else if (hold < 1)
		{
			no();
			break;
		}
	}
		if (ans < 1)
		{
			no();
		}
		system("pause");
		return 0;
}