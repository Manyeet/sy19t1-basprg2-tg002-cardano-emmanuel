#include <iostream>
#include <string>
#include <time.h>
using namespace std;
void bet(int& gold, int& bet)
{
	cout << "Your current gold: " << gold << endl;
	cout << "How much will you wager?" << endl << "Your answer: ";
	cin >> bet;
	if (gold < bet)
	{
		while (gold < bet)
		{
			cout << "Your current gold: " << gold << endl;
			cout << "Insufficient gold to bet." << endl;
			cout << "(Input bet lower or equal to current gold.)" << endl << "Your answer: ";
			cin >> bet;
			if (gold >= bet)
			{
				break;
			}
		}
	}
	if (gold >= bet)
	{
		gold = gold - bet;
		cout << bet << " was deducted to your current gold." << endl;
		cout << "Your gold is now: " << gold << endl;
	}
	system("pause");
}
int roll()
{
	int dice[2] = { 0, 0 };
	for (int i = 0; i < 1; i++)
	{
		int roll = 0;
		roll = rand() % (6 + 1);
		dice[i] = roll;
	}
	int	sum = dice[0] + dice[1];
	return sum;
}
void payout(int& bet, int& gold, int& resP, int& resB)
{
	if (resP > resB)
	{
		gold = gold + bet;
		cout << "Player wins " << gold << " gold." << endl;
	}
	else if (resP == 2 && resB < 2 || resP == 2 && resB > 2)
	{
		gold = gold + (bet * 3);
		cout << "Player wins " << gold << " (3x) gold." << endl;
	}
	else if (resP == resB)
	{
		cout << "It's a draw." << endl;
	}
	else if (resP < resB)
	{
		gold = gold - bet;
		cout << "Player loses " << gold << " gold." << endl;
	}
	system("pause");
}
void playRound(int& gold, int& playerBet, int& resP, int& resB)
{
	while (gold > 0)
	{
		system("CLS");
		bet(gold, playerBet);
		payout(playerBet, gold, resP, resB);
	}
	if (gold <= 0)
	{
		system("CLS");
		cout << "You now have " << gold << " gold." << endl;
		system("pause");
		system("CLS");
		cout << "This program will now exit." << endl;
		system("pause");
	}
}
int main()
{
	srand(time(NULL));
	int gold = 1000, bet = 0, playerResult, botResult;
	playerResult = roll(), botResult = roll();
	playRound(gold, bet, playerResult, botResult);
	return 0;
}