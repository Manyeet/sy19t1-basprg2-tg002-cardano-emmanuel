#include <vector>
#include "Classes.h"

using namespace std;

Warrior::Warrior(int hp)
{
	pName = "Enemy Warrior";
	pHP = 10;
	pPow = rand() % 6 + 4;
	pVit = rand() % 4 + 3;
	pDex = rand() % 6 + 4;
	pAgi = rand() % 7 + 3;
}
Warrior::Warrior(string& name, int hp, int vit)
{
	pName = name;
	pHP = 10;
	pPow = rand() % 6 + 4;
	pVit = rand() % 4 + 3;
	pDex = rand() % 6 + 4;
	pAgi = rand() % 7 + 3;
}
void Warrior::viewStats(vector<string> classes, int i, int hp)
{
	cout << "Name: " << pName << endl;
	cout << "Class: " << classes[i] << endl;
	cout << "HP: " << pHP << "/" << pHP << endl;
	cout << "Pow: " << pPow << endl;
	cout << "Vit: " << pVit << endl;
	cout << "Dex: " << pDex << endl;
	cout << "Agi: " << pAgi << endl;
}
void Warrior::endOfGameStats(vector<string> classes, int i, int pow, int vit, int dex, int agi)
{
	cout << "Name: " << pName << endl;
	cout << "Class: " << classes[i] << endl;
	cout << "HP: 0/0" << endl;
	cout << "Pow: " << pow << endl;
	cout << "Vit: " << vit << endl;
	cout << "Dex: " << dex << endl;
	cout << "Agi: " << agi << endl;
}
void Warrior::takeDamage(int damage)
{
	if (damage < 0) return;

	pHP -= damage;
	if (pHP < 0) pHP = 0;
}
string Warrior::returnName()
{
	return pName;
}
int Warrior::returnHP()
{
	return pHP;
}
int Warrior::returnPow()
{
	return pPow;
}
int Warrior::returnVit()
{
	return pVit;
}
int Warrior::returnDex()
{
	return pDex;
}
int Warrior::returnAgi()
{
	return pAgi;
}
Warrior::~Warrior()
{
	cout << "Warrior: " << this << " is about to be deleted" << endl;
}

Assassin::Assassin(int hp)
{
	pName = "Enemy Assassin";
	pHP = 10;
	pPow = rand() % 6 + 4;
	pVit = rand() % 4 + 3;
	pDex = rand() % 6 + 4;
	pAgi = rand() % 7 + 3;
}
Assassin::Assassin(string& name, int hp, int agi, int dex)
{
	pName = name;
	pHP = 10;
	pPow = rand() % 6 + 4;
	pVit = rand() % 4 + 3;
	pDex = rand() % 6 + 4;
	pAgi = rand() % 7 + 3;
}
void Assassin::viewStats(vector<string> classes, int i, int hp)
{
	cout << "Name: " << pName << endl;
	cout << "Class: " << classes[i] << endl;
	cout << "HP: " << pHP << "/" << pHP << endl;
	cout << "Pow: " << pPow << endl;
	cout << "Vit: " << pVit << endl;
	cout << "Dex: " << pDex << endl;
	cout << "Agi: " << pAgi << endl;
}
void Assassin::endOfGameStats(vector<string> classes, int i, int pow, int vit, int dex, int agi)
{
	cout << "Name: " << pName << endl;
	cout << "Class: " << classes[i] << endl;
	cout << "HP: 0/0" << endl;
	cout << "Pow: " << pow << endl;
	cout << "Vit: " << vit << endl;
	cout << "Dex: " << dex << endl;
	cout << "Agi: " << agi << endl;
}
string Assassin::returnName()
{
	return pName;
}
int Assassin::returnHP()
{
	return pHP;
}
int Assassin::returnPow()
{
	return pPow;
}
int Assassin::returnVit()
{
	return pVit;
}
int Assassin::returnDex()
{
	return pDex;
}
int Assassin::returnAgi()
{
	return pAgi;
}
void Assassin::takeDamage(int damage)
{
	if (damage < 0) return;

	pHP -= damage;
	if (pHP < 0) pHP = 0;
}
Assassin::~Assassin()
{
	cout << "Assassin: " << this << " is about to be deleted" << endl;
}

Mage::Mage(int hp)
{
	pName = "Enemy Mage";
	pHP = 10;
	pPow = rand() % 6 + 4;
	pVit = rand() % 4 + 3;
	pDex = rand() % 6 + 4;
	pAgi = rand() % 7 + 3;
}
Mage::Mage(string& name, int hp, int pow)
{
	pName = name;
	pHP = 10;
	pPow = rand() % 6 + 4;
	pVit = rand() % 4 + 3;
	pDex = rand() % 5 + 4;
	pAgi = rand() % 6 + 3;
}
void Mage::viewStats(vector <string> classes, int i, int hp)
{
	cout << "Name: " << pName << endl;
	cout << "Class: " << classes[i] << endl;
	cout << "HP: " << pHP << "/" << pHP << endl;
	cout << "Pow: " << pPow << endl;
	cout << "Vit: " << pVit << endl;
	cout << "Dex: " << pDex << endl;
	cout << "Agi: " << pAgi << endl;
}
void Mage::endOfGameStats(vector<string> classes, int i, int pow, int vit, int dex, int agi)
{
	cout << "Name: " << pName << endl;
	cout << "Class: " << classes[i] << endl;
	cout << "HP: 0/0" << endl;
	cout << "Pow: " << pow << endl;
	cout << "Vit: " << vit << endl;
	cout << "Dex: " << dex << endl;
	cout << "Agi: " << agi << endl;
}
string Mage::returnName()
{
	return pName;
}
int Mage::returnHP()
{
	return pHP;
}
int Mage::returnPow()
{
	return pPow;
}
int Mage::returnVit()
{
	return pVit;
}
int Mage::returnDex()
{
	return pDex;
}
int Mage::returnAgi()
{
	return pAgi;
}
void Mage::takeDamage(int damage)
{
	if (damage < 0) return;

	pHP -= damage;
	if (pHP < 0) pHP = 0;
}
Mage::~Mage()
{
	cout << "Mage: " << this << " is about to be deleted" << endl;
}