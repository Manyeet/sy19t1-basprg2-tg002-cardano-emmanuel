#include <iostream>
#include <string>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "Classes.h"

using namespace std;

int main()
{
	// DECLARING VARIABLES
	srand(time(NULL));
	string name, enemyName;
	int playerClass = 0, enemyClass = 0, enemyHP = 0, maxHP = 0, hp = 0, vit = 0, agi = 0, dex = 0, pow = 0, j = 0, k = 0;
	int damage = 6, initialDamage = 0, lastStage = 0;

	// VECTORS
	vector<string> classes;
	classes.push_back("Warrior");
	classes.push_back("Assassin");
	classes.push_back("Mage");

	// CHARACTER CREATION
	cout << "Input name for your character: ";
	cin >> name;
	system("CLS");
	
	Warrior* enemyWarrior = new Warrior(enemyHP);
	Assassin* enemyAssassin = new Assassin(enemyHP);
	Mage* enemyMage = new Mage(enemyHP);
	Warrior* playerWarrior = new Warrior(name, hp, vit);
	Assassin* playerAssassin = new Assassin(name, hp, agi, dex);
	Mage* playerMage = new Mage(name, hp, pow);

	// SELECTING CLASS
	cout << "Select your class, " << name << ":" << endl;
	cout << "	[1] " << classes[0] << endl;
	cout << "	[2] " << classes[1] << endl;
	cout << "	[3] " << classes[2]  << endl;
	cout << endl << "Your answer: ";
	cin >> playerClass;
	playerClass = playerClass - 1;
	j = playerClass;

		if (playerClass == 0)
		{
			hp = playerWarrior->returnHP();
			maxHP = hp;
		}
		else if (playerClass == 1)
		{
			hp = playerAssassin->returnHP();
			maxHP = hp;
		}
		else if (playerClass == 2)
		{
			hp = playerMage->returnHP();
			maxHP = hp;
		}

		if (enemyClass == 0)
		{
			enemyHP = playerWarrior->returnHP();
		}
		else if (enemyClass == 1)
		{
			enemyHP = playerAssassin->returnHP();
		}
		else if (enemyClass == 2)
		{
			enemyHP = playerMage->returnHP();
		}

	// SURVIVAL FIGHTING LOOP SESH AND STAGE++
	for (int i = 1; playerWarrior->returnHP(); i++)
	{
		system("CLS");

		if (playerClass == 0)
		{
			maxHP = hp;
		}
		else if (playerClass == 1)
		{
			maxHP = hp;
		}
		else if (playerClass == 2)
		{
			maxHP = hp;
		}

		if (enemyClass == 0)
		{
			enemyHP = playerWarrior->returnHP();
		}
		else if (enemyClass == 1)
		{
			enemyHP = playerAssassin->returnHP();
		}
		else if (enemyClass == 2)
		{
			enemyHP = playerMage->returnHP();
		}

		cout << "Stage: " << i << endl;
		lastStage = i;

		// PLAYER STATS OUTPUT
		if (playerClass == 0)
		{
			playerWarrior->viewStats(classes, j, hp);
		}
		else if (playerClass == 1)
		{
			playerAssassin->viewStats(classes, j, hp);
		}
		else if (playerClass == 2)
		{
			playerMage->viewStats(classes, j, hp);
		}

		cout << endl << "VS" << endl << endl;

		enemyClass = rand() % 3;
		k = enemyClass;

		// ENEMY STATS OUTPUT
		if (enemyClass == 0)
		{
			enemyWarrior->viewStats(classes, k, enemyHP);
			enemyName = enemyWarrior->returnName();
		}
		else if (enemyClass == 1)
		{
			enemyAssassin->viewStats(classes, k, enemyHP);
			enemyName = enemyAssassin->returnName();
		}
		else if (enemyClass == 2)
		{
			enemyMage->viewStats(classes, k, enemyHP);
			enemyName = enemyMage->returnName();
		}

		cout << endl << endl << "Initiating combat..." << endl;
		system("pause");
		system("CLS");

		// HIGHER AGI? Attack first.
		if (playerWarrior->returnAgi() >= enemyWarrior->returnAgi() || playerWarrior->returnAgi() >= enemyAssassin->returnAgi() || playerWarrior->returnAgi() >= enemyMage->returnAgi() ||
			playerAssassin->returnAgi() >= enemyWarrior->returnAgi() || playerAssassin->returnAgi() >= enemyAssassin->returnAgi() || playerAssassin->returnAgi() >= enemyMage->returnAgi() ||
			playerMage->returnAgi() >= enemyWarrior->returnAgi() || playerMage->returnAgi() >= enemyAssassin->returnAgi() || playerMage->returnAgi() >= enemyMage->returnAgi())
		{
			while (playerWarrior->returnHP() > 0 || playerAssassin->returnHP() > 0 || playerMage->returnHP() > 0)
			{
				if (enemyClass == 0 && playerClass == 0)
				{
					damage = (playerWarrior->returnPow() - enemyWarrior->returnVit());
					enemyWarrior->takeDamage(damage);
				}
				else if (enemyClass == 0 && playerClass == 1)
				{
					damage = (playerAssassin->returnPow() - enemyWarrior->returnVit());
					enemyWarrior->takeDamage(damage);
				}
				else if (enemyClass == 0 && playerClass == 2)
				{
					damage = (playerMage->returnPow() - enemyWarrior->returnVit());
					initialDamage = damage / 2;
					damage = initialDamage + damage;
					enemyWarrior->takeDamage(damage);
					cout << name << " deals 50% more damage to " << enemyName << endl;
				}

				else if (enemyClass == 1 && playerClass == 0)
				{
					damage = (playerWarrior->returnPow() - enemyAssassin->returnVit());
					initialDamage = damage / 2;
					damage = initialDamage + damage;
					enemyAssassin->takeDamage(damage);
					cout << name << " deals 50% more damage to " << enemyName << endl;
				}
				else if (enemyClass == 1 && playerClass == 1)
				{
					damage = (playerAssassin->returnPow() - enemyAssassin->returnVit());
					enemyAssassin->takeDamage(damage);
				}
				else if (enemyClass == 1 && playerClass == 2)
				{
					damage = (playerMage->returnPow() - enemyAssassin->returnVit());;
					enemyAssassin->takeDamage(damage);
				}

				else if (enemyClass == 2 && playerClass == 0)
				{
					damage = (playerWarrior->returnPow() - enemyMage->returnVit());
					enemyMage->takeDamage(damage);
				}
				else if (enemyClass == 2 && playerClass == 1)
				{
					initialDamage = (playerAssassin->returnPow() - enemyMage->returnVit());
					initialDamage = damage / 2;
					damage = initialDamage + damage;
					enemyMage->takeDamage(damage);
					cout << name << " deals 50% more damage to " << enemyName << endl;
				}
				else if (enemyClass == 2 && playerClass == 2)
				{
					damage = (playerMage->returnPow() - enemyMage->returnVit());
					enemyMage->takeDamage(damage);
				}

				cout << name << " dealt " << damage << " damage to " << enemyName << endl;
				system("pause");

				if (playerClass == 0 && enemyClass == 0)
				{
					damage = (enemyWarrior->returnPow() - playerWarrior->returnVit());
					playerWarrior->takeDamage(damage);
				}
				else if (playerClass == 0 && enemyClass == 1)
				{
					damage = (enemyAssassin->returnPow() - playerWarrior->returnVit());
					playerWarrior->takeDamage(damage);
				}
				else if (playerClass == 0 && enemyClass == 2)
				{
					initialDamage = (enemyMage->returnPow() - playerWarrior->returnVit());
					initialDamage = damage / 2;
					damage = initialDamage + damage;
					playerWarrior->takeDamage(damage);
					cout << enemyName << " deals 50% more damage to " << enemyName << endl;
				}

				else if (playerClass == 1 && enemyClass == 0)
				{
					initialDamage = (enemyWarrior->returnPow() - playerAssassin->returnVit());
					initialDamage = damage / 2;
					damage = initialDamage + damage;
					playerAssassin->takeDamage(damage);
					cout << name << " deals 50% more damage to " << enemyName << endl;
				}
				else if (playerClass == 1 && enemyClass == 1)
				{
					damage = (enemyAssassin->returnPow() - playerAssassin->returnVit());
					playerAssassin->takeDamage(damage);
				}
				else if (playerClass == 1 && enemyClass == 2)
				{
					damage = (enemyMage->returnPow() - playerAssassin->returnPow());
					playerAssassin->takeDamage(damage);
				}

				else if (playerClass == 2 && enemyClass == 0)
				{
					damage = (enemyWarrior->returnPow() - playerMage->returnVit());
					playerMage->takeDamage(damage);
				}
				else if (playerClass == 2 && enemyClass == 1)
				{
					initialDamage = (enemyAssassin->returnPow() - playerMage->returnVit());
					initialDamage = damage / 2;
					damage = initialDamage + damage;
					playerMage->takeDamage(damage);
					cout << enemyName << " deals 50% more damage to " << name << endl;
				}
				else if (playerClass == 2 && enemyClass == 2)
				{
					damage = (enemyMage->returnPow() - playerMage->returnVit());
					playerMage->takeDamage(damage);
				}
				cout << enemyName << " dealt " << damage << " damage to " << name << endl;
				system("pause");

				if (playerWarrior->returnHP() <= 0 || playerAssassin->returnHP() <= 0 || playerMage->returnHP() <= 0)
				{
					break;
				}

				// STAGE RESULTS - WHILE IN LOOP (Heal 30%, Bonus Stats on Victory)
				if (enemyWarrior->returnHP() <= 0 || enemyAssassin->returnHP() <= 0 || enemyMage->returnHP() <= 0)
				{
					system("CLS");

					if (enemyClass == 0 && playerClass == 0)
					{
						hp = playerWarrior->returnHP() + 3;
						pow = playerWarrior->returnPow();
						vit = playerWarrior->returnVit() + 3;
						dex = playerWarrior->returnDex();
						agi = playerWarrior->returnAgi();
						cout << "Combat ended..." << endl;
						cout << "Increased stats by..." << endl;
						cout << "HP: 3" << endl;
						cout << "Pow: 0" << endl;
						cout << "Vit: 3" << endl;
						cout << "Dex: 0" << endl;
						cout << "Agi: 0" << endl << endl;
						maxHP = maxHP + (maxHP * 0.3);
						hp = hp + maxHP;
						cout << "You are healed for " << maxHP << endl;
					}
					if (enemyClass == 0 && playerClass == 1)
					{
						hp = playerAssassin->returnHP() + 3;
						pow = playerAssassin->returnPow();
						vit = playerAssassin->returnVit() + 3;
						dex = playerAssassin->returnDex();
						agi = playerAssassin->returnAgi();
						cout << "Combat ended..." << endl;
						cout << "Increased stats by..." << endl;
						cout << "HP: 3" << endl;
						cout << "Pow: 0" << endl;
						cout << "Vit: 3" << endl;
						cout << "Dex: 0" << endl;
						cout << "Agi: 0" << endl << endl;
						maxHP = maxHP + (maxHP * 0.3);
						hp = hp + maxHP;
						cout << "You are healed for " << maxHP << endl;
					}
					if (enemyClass == 0 && playerClass == 2)
					{
						hp = playerMage->returnHP() + 3;
						pow = playerMage->returnPow();
						vit = playerMage->returnVit() + 3;
						dex = playerMage->returnDex();
						agi = playerMage->returnAgi();
						cout << "Combat ended..." << endl;
						cout << "Increased stats by..." << endl;
						cout << "HP: 3" << endl;
						cout << "Pow: 0" << endl;
						cout << "Vit: 3" << endl;
						cout << "Dex: 0" << endl;
						cout << "Agi: 0" << endl << endl;
						maxHP = maxHP + (maxHP * 0.3);
						hp = hp + maxHP;
						cout << "You are healed for " << maxHP << endl;
					}
					else if (enemyClass == 1 && playerClass == 0)
					{
						hp = playerWarrior->returnHP();
						pow = playerWarrior->returnPow();
						vit = playerWarrior->returnVit();
						agi = playerWarrior->returnAgi() + 3;
						dex = playerWarrior->returnDex() + 3;
						cout << "Combat ended..." << endl;
						cout << "Increased stats by..." << endl;
						cout << "HP: 0" << endl;
						cout << "Pow: 0" << endl;
						cout << "Vit: 0" << endl;
						cout << "Dex: 3" << endl;
						cout << "Agi: 3" << endl << endl;
						maxHP = maxHP + (maxHP * 0.3);
						hp = hp + maxHP;
						cout << "You are healed for " << maxHP << endl;
					}
					else if (enemyClass == 1 && playerClass == 1)
					{
						hp = playerAssassin->returnHP();
						pow = playerAssassin->returnPow();
						vit = playerAssassin->returnVit();
						agi = playerAssassin->returnAgi() + 3;
						dex = playerAssassin->returnDex() + 3;
						hp = hp + maxHP;
						cout << "Combat ended..." << endl;
						cout << "Increased stats by..." << endl;
						cout << "HP: 0" << endl;
						cout << "Pow: 0" << endl;
						cout << "Vit: 0" << endl;
						cout << "Dex: 3" << endl;
						cout << "Agi: 3" << endl << endl;
						maxHP = maxHP + (maxHP * 0.3);
						hp = hp + maxHP;
						cout << "You are healed for " << maxHP << endl;
					}
					else if (enemyClass == 1 && playerClass == 2)
					{
						hp = playerMage->returnHP();
						pow = playerMage->returnPow();
						vit = playerMage->returnVit();
						agi = playerMage->returnAgi() + 3;
						dex = playerMage->returnDex() + 3;
						cout << "Combat ended..." << endl;
						cout << "Increased stats by..." << endl;
						cout << "HP: 0" << endl;
						cout << "Pow: 0" << endl;
						cout << "Vit: 0" << endl;
						cout << "Dex: 3" << endl;
						cout << "Agi: 3" << endl << endl;
						maxHP = maxHP + (maxHP * 0.3);
						hp = hp + maxHP;
						cout << "You are healed for " << maxHP << endl;
					}
					else if (enemyClass == 2 && playerClass == 0)
					{
						hp = playerWarrior->returnHP();
						pow = playerWarrior->returnPow() + 5;
						vit = playerWarrior->returnVit();
						agi = playerWarrior->returnAgi();
						dex = playerWarrior->returnDex();
						cout << "Combat ended..." << endl;
						cout << "Increased stats by..." << endl;
						cout << "HP: 0" << endl;
						cout << "Pow: 5" << endl;
						cout << "Vit: 0" << endl;
						cout << "Dex: 0" << endl;
						cout << "Agi: 0" << endl << endl;
						maxHP = maxHP + (maxHP * 0.3);
						hp = hp + maxHP;
						cout << "You are healed for " << maxHP << endl;
					}
					else if (enemyClass == 2 && playerClass == 1)
					{
						hp = playerAssassin->returnHP();
						pow = playerAssassin->returnPow() + 5;
						vit = playerAssassin->returnVit();
						agi = playerAssassin->returnAgi();
						dex = playerAssassin->returnDex();
						cout << "Combat ended..." << endl;
						cout << "Increased stats by..." << endl;
						cout << "HP: 0" << endl;
						cout << "Pow: 5" << endl;
						cout << "Vit: 0" << endl;
						cout << "Dex: 0" << endl;
						cout << "Agi: 0" << endl << endl;
						maxHP = maxHP + (maxHP * 0.3);
						hp = hp + maxHP;
						cout << "You are healed for " << maxHP << endl;
					}
					else if (enemyClass == 2 && playerClass == 2)
					{
						hp = playerMage->returnHP();
						pow = playerMage->returnPow() + 5;
						vit = playerMage->returnVit();
						agi = playerMage->returnAgi();
						dex = playerMage->returnDex();
						cout << "Combat ended..." << endl;
						cout << "Increased stats by..." << endl;
						cout << "HP: 0" << endl;
						cout << "Pow: 5" << endl;
						cout << "Vit: 0" << endl;
						cout << "Dex: 0" << endl;
						cout << "Agi: 0" << endl << endl;
						maxHP = maxHP + (maxHP * 0.3);
						hp = hp + maxHP;
						cout << "You are healed for " << maxHP << endl;
					}
					system("pause");
					break;
				}
			}
		}
		else
		{
				while (playerWarrior->returnHP() > 0 || playerAssassin->returnHP() > 0 || playerMage->returnHP() > 0)
				{
					if (playerClass == 0 && enemyClass == 0)
					{
						damage = (enemyWarrior->returnPow() - playerWarrior->returnVit());
						playerWarrior->takeDamage(damage);
					}
					else if (playerClass == 0 && enemyClass == 1)
					{
						damage = (enemyAssassin->returnPow() - playerWarrior->returnVit());
						playerWarrior->takeDamage(damage);
					}
					else if (playerClass == 0 && enemyClass == 2)
					{
						damage = (enemyMage->returnPow() - playerWarrior->returnVit());
						initialDamage = damage / 2;
						damage = initialDamage + damage;
						playerWarrior->takeDamage(damage);
						cout << enemyName << " deals 50% more damage to " << name << endl;
					}

					else if (playerClass == 1 && enemyClass == 0)
					{
						damage = (enemyWarrior->returnPow() - playerAssassin->returnVit());
						initialDamage = damage / 2;
						damage = initialDamage + damage;
						playerAssassin->takeDamage(damage);
						cout << enemyName << " deals 50% more damage to " << name << endl;
					}
					else if (playerClass == 1 && enemyClass == 1)
					{
						damage = (enemyAssassin->returnPow() - playerAssassin->returnVit());
						playerAssassin->takeDamage(damage);
					}
					else if (playerClass == 1 && enemyClass == 2)
					{
						damage = (enemyMage->returnPow() - playerAssassin->returnPow());
						playerAssassin->takeDamage(damage);
					}

					else if (playerClass == 2 && enemyClass == 0)
					{
						damage = (enemyWarrior->returnPow() - playerMage->returnVit());
						playerMage->takeDamage(damage);
					}
					else if (playerClass == 2 && enemyClass == 1)
					{
						damage = (enemyAssassin->returnPow() - playerMage->returnVit());
						initialDamage = damage / 2;
						damage = initialDamage + damage;
						playerMage->takeDamage(damage);
						cout << enemyName << " deals 50% more damage to " << name << endl;
					}
					else if (playerClass == 2 && enemyClass == 2)
					{
						damage = (enemyMage->returnPow() - playerMage->returnVit());
						playerMage->takeDamage(damage);
					}

					cout << enemyName << " dealt " << damage << " damage to " << name << endl;
					system("pause");

					if (enemyClass == 0 && playerClass == 0)
					{
						damage = (playerWarrior->returnPow() - enemyWarrior->returnVit());
						enemyWarrior->takeDamage(damage);
					}
					else if (enemyClass == 0 && playerClass == 1)
					{
						damage = (playerAssassin->returnPow() - enemyWarrior->returnVit());
						enemyWarrior->takeDamage(damage);
					}
					else if (enemyClass == 0 && playerClass == 2)
					{
						damage = (playerMage->returnPow() - enemyWarrior->returnVit());
						initialDamage = damage / 2;
						damage = initialDamage + damage;
						enemyWarrior->takeDamage(damage);
						cout << enemyName << " deals 50% more damage to " << name << endl;
					}

					else if (enemyClass == 1 && playerClass == 0)
					{
						damage = (playerWarrior->returnPow() - enemyAssassin->returnVit());
						initialDamage = damage / 2;
						damage = initialDamage + damage;
						enemyAssassin->takeDamage(damage);
						cout << enemyName << " deals 50% more damage to " << name << endl;
					}
					else if (enemyClass == 1 && playerClass == 1)
					{
						damage = (playerAssassin->returnPow() - enemyAssassin->returnVit());
						enemyAssassin->takeDamage(damage);
					}
					else if (enemyClass == 1 && playerClass == 2)
					{
						damage = (playerMage->returnPow() - enemyAssassin->returnVit());
						enemyAssassin->takeDamage(damage);
					}

					else if (enemyClass == 2 && playerClass == 0)
					{
						damage = (playerWarrior->returnPow() - enemyMage->returnVit());
						enemyMage->takeDamage(damage);
					}
					else if (enemyClass == 2 && playerClass == 1)
					{
						damage = (playerAssassin->returnPow() - enemyMage->returnVit());
						initialDamage = damage / 2;
						damage = initialDamage + damage;
						enemyMage->takeDamage(damage);
						cout << enemyName << " deals 50% more damage to " << name << endl;
					}
					else if (enemyClass == 2 && playerClass == 2)
					{
						damage = (playerMage->returnPow() - enemyMage->returnVit());
						enemyMage->takeDamage(damage);
					}

					cout << name << " dealt " << damage << " damage to " << enemyName << endl;
					system("pause");

					if (playerWarrior->returnHP() <= 0 || playerAssassin->returnHP() <= 0 || playerMage->returnHP() <= 0)
					{
						break;
					}

					// STAGE RESULTS - WHILE IN LOOP (Heal 30%, Bonus Stats on Victory)
					if (enemyWarrior->returnHP() <= 0 || enemyAssassin->returnHP() <= 0 || enemyMage->returnHP() <= 0)
					{
						system("CLS");

						if (enemyClass == 0 && playerClass == 0)
						{
							hp = playerWarrior->returnHP() + 3;
							pow = playerWarrior->returnPow();
							vit = playerWarrior->returnVit() + 3;
							dex = playerWarrior->returnDex();
							agi = playerWarrior->returnAgi();
							cout << "Combat ended..." << endl;
							cout << "Increased stats by..." << endl;
							cout << "HP: 3" << endl;
							cout << "Pow: 0" << endl;
							cout << "Vit: 3" << endl;
							cout << "Dex: 0" << endl;
							cout << "Agi: 0" << endl << endl;
							maxHP = maxHP + (maxHP * 0.3);
							hp = hp + maxHP;
							cout << "You are healed for " << maxHP << endl;
						}
						if (enemyClass == 0 && playerClass == 1)
						{
							hp = playerAssassin->returnHP() + 3;
							pow = playerAssassin->returnPow();
							vit = playerAssassin->returnVit() + 3;
							dex = playerAssassin->returnDex();
							agi = playerAssassin->returnAgi();
							cout << "Combat ended..." << endl;
							cout << "Increased stats by..." << endl;
							cout << "HP: 3" << endl;
							cout << "Pow: 0" << endl;
							cout << "Vit: 3" << endl;
							cout << "Dex: 0" << endl;
							cout << "Agi: 0" << endl << endl;
							maxHP = maxHP + (maxHP * 0.3);
							hp = hp + maxHP;
							cout << "You are healed for " << maxHP << endl;
						}
						if (enemyClass == 0 && playerClass == 2)
						{
							hp = playerMage->returnHP() + 3;
							pow = playerMage->returnPow();
							vit = playerMage->returnVit() + 3;
							dex = playerMage->returnDex();
							agi = playerMage->returnAgi();
							cout << "Combat ended..." << endl;
							cout << "Increased stats by..." << endl;
							cout << "HP: 3" << endl;
							cout << "Pow: 0" << endl;
							cout << "Vit: 3" << endl;
							cout << "Dex: 0" << endl;
							cout << "Agi: 0" << endl << endl;
							maxHP = maxHP + (maxHP * 0.3);
							hp = hp + maxHP;
							cout << "You are healed for " << maxHP << endl;
						}
						else if (enemyClass == 1 && playerClass == 0)
						{
							hp = playerWarrior->returnHP();
							pow = playerWarrior->returnPow();
							vit = playerWarrior->returnVit();
							agi = playerWarrior->returnAgi() + 3;
							dex = playerWarrior->returnDex() + 3;
							cout << "Combat ended..." << endl;
							cout << "Increased stats by..." << endl;
							cout << "HP: 0" << endl;
							cout << "Pow: 0" << endl;
							cout << "Vit: 0" << endl;
							cout << "Dex: 3" << endl;
							cout << "Agi: 3" << endl << endl;
							maxHP = maxHP + (maxHP * 0.3);
							hp = hp + maxHP;
							cout << "You are healed for " << maxHP << endl;
						}
						else if (enemyClass == 1 && playerClass == 1)
						{
							hp = playerAssassin->returnHP();
							pow = playerAssassin->returnPow();
							vit = playerAssassin->returnVit();
							agi = playerAssassin->returnAgi() + 3;
							dex = playerAssassin->returnDex() + 3;
							hp = hp + maxHP;
							cout << "Combat ended..." << endl;
							cout << "Increased stats by..." << endl;
							cout << "HP: 0" << endl;
							cout << "Pow: 0" << endl;
							cout << "Vit: 0" << endl;
							cout << "Dex: 3" << endl;
							cout << "Agi: 3" << endl << endl;
							maxHP = maxHP + (maxHP * 0.3);
							hp = hp + maxHP;
							cout << "You are healed for " << maxHP << endl;
						}
						else if (enemyClass == 1 && playerClass == 2)
						{
							hp = playerMage->returnHP();
							pow = playerMage->returnPow();
							vit = playerMage->returnVit();
							agi = playerMage->returnAgi() + 3;
							dex = playerMage->returnDex() + 3;
							cout << "Combat ended..." << endl;
							cout << "Increased stats by..." << endl;
							cout << "HP: 0" << endl;
							cout << "Pow: 0" << endl;
							cout << "Vit: 0" << endl;
							cout << "Dex: 3" << endl;
							cout << "Agi: 3" << endl << endl;
							maxHP = maxHP + (maxHP * 0.3);
							hp = hp + maxHP;
							cout << "You are healed for " << maxHP << endl;
						}
						else if (enemyClass == 2 && playerClass == 0)
						{
							hp = playerWarrior->returnHP();
							pow = playerWarrior->returnPow() + 5;
							vit = playerWarrior->returnVit();
							agi = playerWarrior->returnAgi();
							dex = playerWarrior->returnDex();
							cout << "Combat ended..." << endl;
							cout << "Increased stats by..." << endl;
							cout << "HP: 0" << endl;
							cout << "Pow: 5" << endl;
							cout << "Vit: 0" << endl;
							cout << "Dex: 0" << endl;
							cout << "Agi: 0" << endl << endl;
							maxHP = maxHP + (maxHP * 0.3);
							hp = hp + maxHP;
							cout << "You are healed for " << maxHP << endl;
						}
						else if (enemyClass == 2 && playerClass == 1)
						{
							hp = playerAssassin->returnHP();
							pow = playerAssassin->returnPow() + 5;
							vit = playerAssassin->returnVit();
							agi = playerAssassin->returnAgi();
							dex = playerAssassin->returnDex();
							cout << "Combat ended..." << endl;
							cout << "Increased stats by..." << endl;
							cout << "HP: 0" << endl;
							cout << "Pow: 5" << endl;
							cout << "Vit: 0" << endl;
							cout << "Dex: 0" << endl;
							cout << "Agi: 0" << endl << endl;
							maxHP = maxHP + (maxHP * 0.3);
							hp = hp + maxHP;
							cout << "You are healed for " << maxHP << endl;
						}
						else if (enemyClass == 2 && playerClass == 2)
						{
							hp = playerMage->returnHP();
							pow = playerMage->returnPow() + 5;
							vit = playerMage->returnVit();
							agi = playerMage->returnAgi();
							dex = playerMage->returnDex();
							cout << "Combat ended..." << endl;
							cout << "Increased stats by..." << endl;
							cout << "HP: 0" << endl;
							cout << "Pow: 5" << endl;
							cout << "Vit: 0" << endl;
							cout << "Dex: 0" << endl;
							cout << "Agi: 0" << endl << endl;
							maxHP = maxHP + (maxHP * 0.3);
							hp = hp + maxHP;
							cout << "You are healed for " << maxHP << endl;
						}
						system("pause");
						break;
					}
				}
		}
	} // FOR LOOP ENDS HERE

	// PLAYER DEAD? END LOOP AND THEN SHOW END OF GAME RESULTS
	if (playerWarrior->returnHP() <= 0 || playerAssassin->returnHP() <= 0 || playerMage->returnHP() <= 0)
	{
		system("CLS");
		cout << "Stage Reached: " << lastStage << endl;
		cout << "Player's Stats" << endl;
		if (playerClass == 0)
		{
			playerWarrior->endOfGameStats(classes, j, pow, vit, dex, agi);
		}
		else if (playerClass == 1)
		{
			playerAssassin->endOfGameStats(classes, j, pow, vit, dex, agi);
		}
		else if (playerClass == 2)
		{
			playerMage->endOfGameStats(classes, j, pow, vit, dex, agi);
		}
	}

	system("pause");
	return 0;
}