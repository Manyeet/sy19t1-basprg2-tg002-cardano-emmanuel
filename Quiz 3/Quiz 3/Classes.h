#include <string>
#include <iostream>
#include <vector>

using namespace std;

#pragma once
class Warrior
{
public:
	// Setter
	Warrior(int hp);
	Warrior(string& name, int hp, int vit);
	~Warrior();

	// Getter
	void viewStats(vector<string> classes, int i, int hp);
	void endOfGameStats(vector<string> classes, int i, int pow, int vit, int dex, int agi);
	string returnName();
	int returnHP();
	int returnPow();
	int returnVit();
	int returnDex();
	int returnAgi();
	
	void takeDamage(int damage);
private:
	string pName;
	int pHP;
	int pPow;
	int pVit;
	int pDex;
	int pAgi;
};

class Assassin
{
public:
	// Setter
	Assassin(int hp);
	Assassin(string& name, int hp, int agi, int dex);
	~Assassin();

	// Getter
	void viewStats(vector<string> classes, int i, int hp);
	void endOfGameStats(vector<string> classes, int i, int pow, int vit, int dex, int agi);
	string returnName();
	int returnHP();
	int returnPow();
	int returnVit();
	int returnDex();
	int returnAgi();

	void takeDamage(int damage);
private:
	string pName;
	int pHP;
	int pPow;
	int pVit;
	int pDex;
	int pAgi;
};

class Mage
{
public:
	// Setter
	Mage(int hp);
	Mage(string& name, int hp, int pow);
	~Mage();

	// Getter
	void viewStats(vector<string> classes, int i, int hp);
	void endOfGameStats(vector<string> classes, int i, int pow, int vit, int dex, int agi);
	string returnName();
	int returnHP();
	int returnPow();
	int returnVit();
	int returnDex();
	int returnAgi();

	void takeDamage(int damage);
private:
	string pName;
	int pHP;
	int pPow;
	int pVit;
	int pDex;
	int pAgi;
};