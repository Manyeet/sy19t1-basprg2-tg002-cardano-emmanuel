#include <iostream>
#include <string>
#include <time.h>
#include <stdlib.h>


using namespace std;

void printArray(string items[])
{
	for (int i = 0; i < 8; i++)
	{
		cout << items[i] << endl;
	}
}

int main()
{
	string items[8] = { "Red Potion", "Blue Potion", "Yggdrasil Leaf", "Elixir", "Teleport Scroll", "Red Potion", "Red Potion", "Elixir" };
	printArray(items);
	cout << endl;
	system("pause");
	return 0;
}