#include <vector>
#include <iostream>
#include <string>
#include <time.h>

using namespace std;

int randOpp(int& numberofInstances)
{
	int random = rand() % numberofInstances + 1;
	return random;
}
void startUp(int& Cash, int& distanceLeft, bool& empTrue, int& mmWager, int& i)
{
	cout << "Cash: " << Cash << endl;
	cout << "Distance left (mm): " << distanceLeft << endl;
	cout << "Round: " << i << "/12" << endl;
	if (i <= 3 || i == 7 || i == 8 || i == 9)
	{
		cout << "Side: " << "Emperor" << endl << endl;
		empTrue = 1;
	}
	else
	{
		cout << "Side: " << "Slave" << endl << endl;
		empTrue = 0;
	}
	cout << "How many milimeters would you like to wager? ";
	cin >> mmWager;
}
void pickACard(vector<string>& vector, int& numberofInstances, int& inputNum, int& i)
{
	cout << "Pick a card to play:" << endl << "======================" << endl;
	if (i <= 3 || i == 7 || i == 8 || i == 9)
	{
		cout << "[1]" << " Emperor" << endl;
	}
	else
	{
		cout << "[1]" << " Slave" << endl;
	}
	for (int i = 2; i <= numberofInstances; i++)
	{
		cout << "[" << i << "] " << vector[i] << endl;
	}
	cout << endl << "Your answer: ";
	cin >> inputNum;
}
void game(int& Cash, int& distanceLeft, vector<string> vector)
{
	int inputNum = 0, numberofInstances = 5, mmWager = 0;
	bool empTrue = 0;
	for (int i = 1; i <= 12; i++)
	{
		// Start
		system("CLS");
		startUp(Cash, distanceLeft, empTrue, mmWager, i);
		while (mmWager <= 0)
		{
			cout << "Please input values ranging from 1 to 30." << endl;
			system("pause");
			system("CLS");
			startUp(Cash, distanceLeft, empTrue, mmWager, i);
		}
		system("CLS");
		// Pick a card part
		pickACard(vector, numberofInstances, inputNum, i);
		system("CLS");
		// Conditions
		if (empTrue == 1 && inputNum == 1 && randOpp(numberofInstances) >= 2)
		{
			cout << "[Player] " << " Emperor" << " vs [Opponent] " << "Civilian" << endl;
			cout << endl << "You won " << 100000 * mmWager << " yen." << endl << endl;
			Cash = Cash + 100000 * mmWager;
		}
		else if (empTrue == 1 && inputNum == 1 && randOpp(numberofInstances) == 1)
		{
			cout << "[Player] " << "Emperor" << " vs [Opponent] " << "Slave" << endl;
			cout << endl << "You lost! The pin will now move by " << mmWager << " mm." << endl << endl;
			distanceLeft = distanceLeft - mmWager;
		}
		else if (empTrue == 0 && inputNum == 1 && randOpp(numberofInstances) == 1)
		{
			cout << "[Player] " << "Slave" << " vs [Opponent] " << "Emperor" << endl;
			cout << endl << "You won " << "100000 x " << mmWager << " x 5 = " << 100000 * mmWager * 5 << " yen." << endl;
			Cash = Cash + 100000 * mmWager * 5;
		}
		else if (empTrue == 0 && inputNum == 1 && randOpp(numberofInstances) >= 2)
		{
			cout << "[Player] " << "Slave" << " vs [Opponent] " << "Citizen" << endl;
			cout << endl << "You lost! The pin will now move by " << mmWager << " mm." << endl << endl;
			distanceLeft = distanceLeft - mmWager;
		}
		else while (inputNum >= 2 && randOpp(numberofInstances) >= 2)
		{
			cout << "[Player] " << "Civilian" << " vs [Opponent] " << "Civilian" << endl;
			cout << endl << "Draw!" << endl;
			vector.erase(vector.begin() + numberofInstances);
			numberofInstances = numberofInstances--;
			system("pause");
			system("CLS");
			pickACard(vector, numberofInstances, inputNum, i);
			system("CLS");
			if (empTrue == 1 && inputNum >= 2 && randOpp(numberofInstances) == 1)
			{
				cout << "[Player] " << "Civilian" << " vs [Opponent] " << "Slave" << endl;
				cout << endl << "You won " << 100000 * mmWager << " yen." << endl << endl;
				Cash = Cash + 100000 * mmWager;
				break;
			}
			else if (empTrue == 0 && inputNum >= 2 && randOpp(numberofInstances) == 1)
			{
				cout << "[Player] " << "Civilian" << " vs [Opponent] " << "Emperor" << endl;
				cout << endl << "You lost! The pin will now move by " << mmWager << " mm." << endl << endl;
				distanceLeft = distanceLeft - mmWager;
				break;
			}
		}
		if (distanceLeft <= 0)
		{
			break;
		}
		system("pause");
	}
}
void gameEnd(int& Cash, int& distanceLeft)
{
	system("CLS");
	if (Cash >= 20000000 && distanceLeft <= 30) // Best Ending
	{
		cout << "Congratulations!!!" << endl << "You got the Best Ending!" << endl;
		system("pause");
		system("CLS");
		cout << "Congratulations!!!" << endl << "You got the Best Ending!" << endl;
		cout << "You have won " << Cash << " yen!!!" << endl;
		system("pause");
		system("CLS");
		cout << "The End." << endl;
		system("pause");
	}
	else if (Cash < 20000000 && distanceLeft <= 30) // Meh Ending
	{
		cout << "Pfft. Boooooring." << endl;
		cout << "You have acquired " << Cash << " yen." << endl;
		system("pause");
		system("CLS");
		cout << "The End." << endl;
		system("pause");
	}
	else if (distanceLeft <= 0) // Bad Ending
	{
		cout << "The pin has pierced your eardrum." << endl;
		system("pause");
		system("CLS");
		cout << "The pin has pierced your eardrum." << endl;
		cout << "You have now acquired the Bad Ending." << endl;
		system("pause");
		system("CLS");
		cout << "The pin has pierced your eardrum." << endl;
		cout << "You have now acquired the Bad Ending." << endl;
		cout << "Why not try again?" << endl;
		system("pause");
	}
}
int main()
{
	srand(time(NULL));
	// Declarations
	int Cash = 0, distanceLeft = 30;
	vector<string> vector;
	vector.push_back("Emperor");  // 0
	vector.push_back("Slave");	  // 1
	vector.push_back("Civilian"); // 2
	vector.push_back("Civilian"); // 3
	vector.push_back("Civilian"); // 4
	vector.push_back("Civilian"); // 5

	// Game Loop
	game(Cash, distanceLeft, vector);
	// Game Ending
	gameEnd(Cash, distanceLeft);
	return 0;
}