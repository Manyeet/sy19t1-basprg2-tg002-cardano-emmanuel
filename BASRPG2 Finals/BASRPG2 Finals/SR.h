#pragma once
#include "Item.h"
#include <string>
#include <iostream>

using namespace std;

class SR : public Item
{
public:
	SR(string name, int value, string ItemFunction);

private:
	string mItemFunction = " Rarity Points";
	int mValue = 10;
};