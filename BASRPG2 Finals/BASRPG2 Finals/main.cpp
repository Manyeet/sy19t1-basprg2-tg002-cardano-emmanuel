#include <iostream>
#include <string>
#include <time.h>
#include "Player.h"
#include "Item.h"
#include "SSR.h"
#include "SR.h"
#include "R.h"
#include "HP.h"
#include "Bomb.h"
#include "Crystal.h"

using namespace std;

int main()
{
	srand(time(NULL));
	Player p1;
	int randomNum = 0;
	Item* i1 = new Item("test", 0, "test");

	while (p1.getHP() > 0 && p1.getNumOfRarityPoints() < 100 && p1.getCrystals() > 0)
	{
		randomNum = i1->getDrawChance();

		if (randomNum == 1) // SSR 1%
		{ 
			i1 = new SSR("SSR", 50, " Rarity Points");
			p1.displayStatsRP(p1, i1);
			i1->displayItemPulls(i1);
			p1.ssrCounter();
		}
		else if (randomNum >= 2 && randomNum <= 10) // SR 9%
		{ 
			i1 = new SR("SR", 10, " Rarity Points");
			p1.displayStatsRP(p1, i1);
			i1->displayItemPulls(i1);
			p1.srCounter();
		}
		else if (randomNum >= 11 && randomNum <= 51) // R 40%
		{
			i1 = new R("R", 1, " Rarity Points");
			p1.displayStatsRP(p1, i1);
			i1->displayItemPulls(i1);
			p1.rCounter();
		}
		else if (randomNum >= 52 && randomNum <= 67) // HP 15%
		{
			i1 = new HP("Health Potion", 30, " Healing");
			p1.displayStatsHP(p1);
			i1->displayItemPulls(i1);
		}
		else if (randomNum >= 68 && randomNum <= 88) // Bomb 20%
		{
			i1 = new Bomb("Bomb", 25, " damage");
			p1.displayStatsDmg(p1);
			i1->displayItemPulls(i1);
		}
		else if (randomNum >= 89) // Crystal 15%
		{
			i1 = new Crystal("Crystal", 15, " crystals");
			p1.displayStatsC(p1);
			i1->displayItemPulls(i1);
		}
		system("pause");
		system("CLS");
	}
	if (p1.getNumOfRarityPoints() >= 100) // WIN Condition
	{
		cout << "You actually won!" << endl << "==================================" << endl;
		p1.displayStats(p1);
		cout << "==================================" << endl << endl;

		p1.displayItemResults();
		system("pause");
		return 0;
	}
	else // LOSE Condition
	{
		cout << "You lost!" << endl << "==================================" << endl;
		p1.displayStats(p1);
		cout << "==================================" << endl << endl;

		p1.displayItemResults();
		system("pause");
		return 0;
	}
}
