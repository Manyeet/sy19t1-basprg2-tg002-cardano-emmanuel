#include <iostream>
#include "Item.h"
#include "Player.h"

using namespace std;

Player::Player()
{
}

void Player::displayStats(Player p1)
{
	cout << "HP: " << p1.getHP() << endl;
	cout << "Crystals: " << p1.getCrystals() << endl;
	cout << "Rarity Points: " << p1.getNumOfRarityPoints() << endl;
	cout << "Pulls: " << p1.getPull() << endl;
}

void Player::displayStatsRP(Player p1, Item* i1)
{
	cout << "HP: " << p1.getHP() << endl;
	cout << "Crystals: " << p1.getCrystals() << endl;
	cout << "Rarity Points: " << p1.getNumOfRarityPoints() << endl;
	cout << "Pulls: " << p1.getPull() << endl;
	addPull(), useCrystals(), addRarityPoints(i1);
}

void Player::displayStatsHP(Player p1)
{
	cout << "HP: " << p1.getHP() << endl;
	cout << "Crystals: " << p1.getCrystals() << endl;
	cout << "Rarity Points: " << p1.getNumOfRarityPoints() << endl;
	cout << "Pulls: " << p1.getPull() << endl;
	addPull(), useCrystals(), healHP(), hPotCounter();
}

void Player::displayStatsDmg(Player p1)
{
	cout << "HP: " << p1.getHP() << endl;
	cout << "Crystals: " << p1.getCrystals() << endl;
	cout << "Rarity Points: " << p1.getNumOfRarityPoints() << endl;
	cout << "Pulls: " << p1.getPull() << endl;
	addPull(), useCrystals(), takeDamage(), bmbCounter();
}

void Player::displayStatsC(Player p1)
{
	cout << "HP: " << p1.getHP() << endl;
	cout << "Crystals: " << p1.getCrystals() << endl;
	cout << "Rarity Points: " << p1.getNumOfRarityPoints() << endl;
	cout << "Pulls: " << p1.getPull() << endl;
	addPull(), useCrystals(), addCrystals(), cCounter();
}

int Player::getHP()
{
	if (hp <= 0) { hp = 0; }
	return hp;
}

void Player::setHP(int hp)
{
	if (hp <= 0) { hp = 0; }
	this->hp = hp;
}

int Player::addRarityPoints(Item * i1)
{
	NumOfRarityPoints += i1->getItemNumber();
	return NumOfRarityPoints;
}

int Player::getNumOfRarityPoints()
{
	return NumOfRarityPoints;
}

void Player::setNumOfRarityPoints(int NumOfRarityPoints)
{
	this->NumOfRarityPoints = NumOfRarityPoints;
}

int Player::getCrystals()
{
	return crystals;
}

int Player::addCrystals()
{
	crystals += 15;
	return crystals;
}

int Player::useCrystals()
{
	this->crystals -= 5;
	return crystals;
}

void Player::setCrystals(int crystals)
{
	this->crystals = crystals;
}

int Player::getPull()
{
	return pull;
}

void Player::setPull(int pull)
{
	this->pull = pull;
}

int Player::addPull()
{
	pull += 1;
	return pull;
}

int Player::takeDamage()
{
	hp -= damage;
	return hp;
}

int Player::healHP()
{
	hp += heal;
	return hp;
}

int Player::ssrCounter()
{
	ssr += 1;
	return ssr;
}

int Player::srCounter()
{
	sr += 1;
	return sr;
}

int Player::rCounter()
{
	r += 1;
	return r;
}

int Player::hPotCounter()
{
	hPot += 1;
	return hPot;
}

int Player::bmbCounter()
{
	bmb += 1;
	return bmb;
}

int Player::cCounter()
{
	c += 1;
	return c;
}

void Player::displayItemResults()
{
	cout << "Items Pulled:" << endl << "----------------------------------" << endl;
	cout << "Bomb " << "x" << bmb << endl;
	cout << "SR " << "x" << sr << endl;
	cout << "R " << "x" << r << endl;
	cout << "Health Potion " "x" << hPot << endl;
	cout << "Crystals " << "x" << c << endl;
}
