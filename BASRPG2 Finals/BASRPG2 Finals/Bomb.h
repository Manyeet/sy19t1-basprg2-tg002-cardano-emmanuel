#pragma once
#include "Item.h"
#include <string>
#include <iostream>

using namespace std;

class Bomb : public Item
{
public:
	Bomb(string name, int value, string ItemFunction);

private:
	string mItemFunction = " damage";
	int mValue = 25;
};
