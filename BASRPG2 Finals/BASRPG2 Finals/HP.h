#pragma once
#include "Item.h"
#include <string>
#include <iostream>

using namespace std;

class HP : public Item
{
public:
	HP(string name, int value, string ItemFunction);

private:
	string mItemFunction = " Healing";
	int mValue = 10;
};
