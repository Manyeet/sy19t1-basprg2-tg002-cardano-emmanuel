#include <string>
#include <iostream>
#include "Item.h"

using namespace std;

Item::Item(string name, int value, string ItemFunction)
{
	mName = name;
	mValue = value;
	mItemFunction = ItemFunction;
}

string Item::getItemName()
{
	return mName;
}

void Item::setItemName(string name)
{
	mName = name;
}

int Item::getItemNumber()
{
	return mValue;
}

void Item::setItemNumber(int value)
{
	mValue = value;
}

string Item::getItemFunction()
{
	return string (mItemFunction);
}

void Item::setItemFunction(string ItemFunction)
{
	mItemFunction = ItemFunction;
}

int Item::getDrawChance()
{
	int i = rand() % 100 + 1;
	return i;
}

void Item::displayItemPulls(Item * i1)
{
	cout << endl << endl << "You pulled " << i1->getItemName() << endl;
	cout << "You have received " << i1->getItemNumber() << i1->getItemFunction() << endl << endl;
	mValue = i1->getItemNumber();
}

