#include <iostream>
#include <time.h>

using namespace std;

class Player
{
private:
	int hp;
	int NumOfRarityPoints;
	int crystals;

public:
	int getHP() { return hp; }
	void setHP(int hp) { this->hp = hp; }
	int getNumOfRarityPoints() { return NumOfRarityPoints; }
	void setNumOfRarityPoints(int NumOfRarityPoints) { this->NumOfRarityPoints = NumOfRarityPoints; }
	int getCrystals() { return crystals; }
	void setCrystals(int crystals) { this->crystals = crystals; }
};

int main()
{
	srand(time(NULL));
	int pull = 0;
	Player p1;
	p1.setHP(100);
	p1.setNumOfRarityPoints(0);
	while (p1.getHP() > 0 && p1.getNumOfRarityPoints() < 100)
	{
		cout << "HP: " << p1.getHP() << endl;
		cout << "Crystals: " << p1.getHP() << endl;
		cout << "Rarity Points: " << p1.getNumOfRarityPoints() << endl;
		cout << "Pulls: " << pull << endl;
		pull += 1;

		cout << "You pulled <item>" << endl;
		cout << "You received <damage or RP>" << endl << endl;
		system("pause");
		system("CLS");
	}
}