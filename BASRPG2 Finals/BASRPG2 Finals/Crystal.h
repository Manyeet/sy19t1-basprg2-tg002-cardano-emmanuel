#pragma once
#include "Item.h"
#include <string>
#include <iostream>

using namespace std;

class Crystal : public Item
{
public:
	Crystal(string name, int value, string ItemFunction);

private:
	string mItemFunction = " crystals";
	int mValue = 15;
};
