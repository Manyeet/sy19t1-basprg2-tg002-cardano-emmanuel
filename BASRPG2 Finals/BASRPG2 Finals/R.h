#include "Item.h"
#include <string>
#include <iostream>

using namespace std;

class R : public Item
{
public:
	R(string name, int value, string ItemFunction);

private:
	string mItemFunction = " Rarity Points";
	int mValue = 1;
};