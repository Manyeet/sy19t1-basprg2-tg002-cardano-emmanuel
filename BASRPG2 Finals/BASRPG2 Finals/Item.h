#pragma once
#include <string>

using namespace std;

class Item
{
public:
	Item(string name, int value, string ItemFunction);
	virtual string getItemName();
	virtual void setItemName(string name);
	virtual int getItemNumber();
	virtual void setItemNumber(int value);
	virtual string getItemFunction();
	virtual void setItemFunction(string ItemFunction);
	virtual int getDrawChance();
	void displayItemPulls(Item * i1);

private:
	string mName, mItemFunction;
	int mValue = 0;
};