#pragma once
#include "Item.h"
class Player
{
public:
	Player();
	void displayStats(Player p1);
	void displayStatsRP(Player p1, Item* i1);
	void displayStatsHP(Player p1);
	void displayStatsDmg(Player p1);
	void displayStatsC(Player p1);
	int getHP();
	void setHP(int hp);
	int addRarityPoints(Item * i1);
	int getNumOfRarityPoints();
	void setNumOfRarityPoints(int NumOfRarityPoints);
	int getCrystals();
	int addCrystals();
	int useCrystals();
	void setCrystals(int crystals);
	int getPull();
	void setPull(int pull);
	int addPull();
	int takeDamage();
	int healHP();
	int ssrCounter();
	int srCounter();
	int rCounter();
	int hPotCounter();
	int bmbCounter();
	int cCounter();
	void displayItemResults();

private:
	int hp = 100, heal = 30, NumOfRarityPoints = 0;
	int crystals = 100, damage = 25, pull = 0;
	int ssr = 0, sr = 0, r = 0, hPot = 0, bmb = 0, c = 0;
};