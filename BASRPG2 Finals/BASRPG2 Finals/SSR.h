#pragma once
#include "Item.h"
#include <string>
#include <iostream>

using namespace std;

class SSR : public Item
{
public:
	SSR(string name, int value, string ItemFunction);

private:
	string mItemFunction = " Rarity Points";
	int mValue = 50;
};