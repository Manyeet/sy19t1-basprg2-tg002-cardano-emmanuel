#include <iostream>
#include <string>
#include <windows.h>
using namespace std;

struct Character
{
	string name;
	int EXP, LVL, HP, maxHP, MP, maxMP, strength, dexterity, vitality, magic, spirit, luck;
	int atk, atkPercent, def, defPercent, magicAtk, magicDef, magicDefPercent;
};

void PrintCharacter(Character character) // basically COLORS
{
	HANDLE h = GetStdHandle(STD_OUTPUT_HANDLE);
	cout << character.name << "				EXP:		" << character.EXP << "p" << endl;
	SetConsoleTextAttribute(h, FOREGROUND_BLUE | FOREGROUND_INTENSITY);
	cout << "LV ";
	SetConsoleTextAttribute(h, FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED);
	cout << character.LVL << endl;
	SetConsoleTextAttribute(h, FOREGROUND_BLUE | FOREGROUND_INTENSITY);
	cout << "HP ";
	SetConsoleTextAttribute(h, FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED);
	cout << character.HP << "/ " << character.maxHP << "			next level:			0p" << endl;
	SetConsoleTextAttribute(h, FOREGROUND_BLUE | FOREGROUND_INTENSITY);
	cout << "MP   ";
	SetConsoleTextAttribute(h, FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED);
	cout << character.MP << "/ " << character.maxMP << "			Limit level:" << 4 << endl;
	SetConsoleTextAttribute(h, FOREGROUND_BLUE | FOREGROUND_INTENSITY);
	cout << "Strength	  ";
	SetConsoleTextAttribute(h, FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED);
	cout << character.strength << endl;
	SetConsoleTextAttribute(h, FOREGROUND_BLUE | FOREGROUND_INTENSITY);
	cout << "Dexterity	  ";
	SetConsoleTextAttribute(h, FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED);
	cout << character.dexterity << endl;
	SetConsoleTextAttribute(h, FOREGROUND_BLUE | FOREGROUND_INTENSITY);
	cout << "Vitality	  ";
	SetConsoleTextAttribute(h, FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED);
	cout << character.vitality << endl;
	SetConsoleTextAttribute(h, FOREGROUND_BLUE | FOREGROUND_INTENSITY);
	cout << "Magic		  ";
	SetConsoleTextAttribute(h, FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED);
	cout << character.magic << endl;
	SetConsoleTextAttribute(h, FOREGROUND_BLUE | FOREGROUND_INTENSITY);
	cout << "Spirit		  ";
	SetConsoleTextAttribute(h, FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED);
	cout << character.spirit << endl;
	SetConsoleTextAttribute(h, FOREGROUND_BLUE | FOREGROUND_INTENSITY);
	cout << "Luck		  ";

	SetConsoleTextAttribute(h, FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED);
	cout << character.luck << endl << endl;
	SetConsoleTextAttribute(h, FOREGROUND_BLUE | FOREGROUND_INTENSITY);
	cout << "Attack		  ";
	SetConsoleTextAttribute(h, FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED);
	cout << character.atk << endl;
	SetConsoleTextAttribute(h, FOREGROUND_BLUE | FOREGROUND_INTENSITY);
	cout << "Attack%		  ";
	SetConsoleTextAttribute(h, FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED);
	cout << character.atkPercent << endl;
	SetConsoleTextAttribute(h, FOREGROUND_BLUE | FOREGROUND_INTENSITY);
	cout << "Defense 	  ";
	SetConsoleTextAttribute(h, FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED);
	cout << character.def << endl;
	SetConsoleTextAttribute(h, FOREGROUND_BLUE | FOREGROUND_INTENSITY);
	cout << "Defense% 	   ";
	SetConsoleTextAttribute(h, FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED);
	cout << character.defPercent << endl;
	SetConsoleTextAttribute(h, FOREGROUND_BLUE | FOREGROUND_INTENSITY);
	cout << "Magic atk	   ";
	SetConsoleTextAttribute(h, FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED);
	cout << character.defPercent << endl;
	SetConsoleTextAttribute(h, FOREGROUND_BLUE | FOREGROUND_INTENSITY);
	cout << "Magic def	  ";
	SetConsoleTextAttribute(h, FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED);
	cout << character.magicDef << endl;
	SetConsoleTextAttribute(h, FOREGROUND_BLUE | FOREGROUND_INTENSITY);
	cout << "Magic def%	    ";
	SetConsoleTextAttribute(h, FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED);
	cout << character.magicDefPercent << endl << endl;
}

int main()
{
	Character c9;

	// Values
	c9.name = "Cloud";
	c9.EXP = 5944665;
	c9.LVL = 99;
	c9.HP = 5935;
	c9.maxHP = 8759;
	c9.MP = 877;
	c9.maxMP = 920;
	c9.strength = 253;
	c9.dexterity = 255;
	c9.vitality = 143;
	c9.magic = 173;
	c9.spirit = 181;
	c9.luck = 197;
	c9.atk = 255;
	c9.atkPercent = 110;
	c9.def = 149;
	c9.defPercent = 66;
	c9.magicAtk = 173;
	c9.magicDef = 181;
	c9.magicDefPercent = 3;

	// Callouts
	PrintCharacter(c9);
	system("pause");
}