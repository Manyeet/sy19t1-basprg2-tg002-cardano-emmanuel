#include <iostream>
#include <string>
#define NODE_H

using namespace std;

struct Node
{
	string name;
	Node* next;
};
void printLinkedList(Node* head)
{
	Node* current = head;
	while (current != NULL)
	{
		cout << current->name << endl;
		current = current->next;
	}
}
void rounds()
{
	for (int i = 0; i < 4; i++)
	{ 
		cout << "==========================================" << endl << "ROUND " << i + 1 << endl << "==========================================";
		cout << endl << "Remaining Members:" << endl;
	}
}
int main()
{
	// when DELETING: (SAMPLE)
	/*	node1 -> next = node2 -> next;
		delete node2;
		node2 = NULL;
	*/
	Node* node1 = new Node;

	Node* node2 = new Node;
	node1->next = node2;

	Node* node3 = new Node;
	node2->next = node3;
	
	Node* node4 = new Node;
	node3->next = node4;

	Node* node5 = new Node;
	node4->next = node5;
	node5->next = node1;

	string name;

	// Soldier Name Loop
	cout << "What is your name soldier? ";
	cin >> name;
	cout << "What is your name soldier? ";
	cin >> name;
	cout << "What is your name soldier? ";
	cin >> name;
	cout << "What is your name soldier? ";
	cin >> name;
	cout << "What is your name soldier? ";
	cin >> name;
	system("CLS");
	// ROUNDS 1 to 4
	rounds();
	// FINAL RESULT
	cout << "==========================================" << endl << "FINAL RESULT" << endl << "==========================================" << endl;
	cout << name << " will go to seek for reinforcements." << endl << endl;
	system("pause");
	return 0;
}